pub enum SlidingState {
    Increasing,
    Decreasing,
}

pub struct SlidingAnimation {
    active: bool,
    state: SlidingState,
    size: f32,
    // TODO: Make those two constants
    max_size: f32,
    speed: f32,
}
impl SlidingAnimation {
    pub fn new(speed: f32, max_size: f32, state: SlidingState) -> Self {
        Self {
            active: false,
            state,
            size: 0.,
            max_size,
            speed,
        }
    }

    pub fn start_animation(&mut self) {
        self.active = true;
    }

    pub fn increment(&mut self) {
        self.size += self.speed;
        if self.size > self.max_size {
            self.size = self.max_size;
            self.active = false;
            self.state = SlidingState::Decreasing;
        }
    }

    pub fn decrement(&mut self) {
        if self.size >= self.speed {
            self.size -= self.speed;
        } else {
            self.active = false;
            self.state = SlidingState::Increasing;
        }
    }

    pub fn size(&self) -> f32 {
        self.size
    }

    pub fn change_size(&mut self) {
        if self.active {
            match self.state {
                SlidingState::Increasing => self.increment(),
                SlidingState::Decreasing => self.decrement(),
            }
        }
    }

    pub fn set_size(&mut self, size: f32) {
        self.size = size;
    }

    pub fn is_active(&self) -> bool {
        self.active
    }
}
