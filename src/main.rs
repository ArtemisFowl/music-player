// TODO: Explain magic constants like 1000 and 100 better (wrap them in consts)
mod playlist;
mod util;
use music_player_styles::{ButtonStyle, SvgStyle, Theme};
use playlist::{name_from_path, PlayerState, Playlist};
use util::{SlidingAnimation, SlidingState};

use dirs;
use iced::{
    executor,
    widget::{button, column, container, row, scrollable, text, Column, Rule, Slider, Space, Svg},
    Alignment, Application, Command, Length, Settings, Subscription,
};
use iced_aw::{split, Card, Modal, Split};
use iced_graphics::Renderer;
use iced_wgpu::Backend;
use native_dialog::FileDialog;
use rodio::{OutputStream, OutputStreamHandle, Sink, Source};
use ron;
use std::{
    collections::{BTreeMap, BTreeSet},
    error::Error,
    ffi::OsStr,
    fmt::Debug,
    fs::{read_to_string, write, File},
    path::PathBuf,
    time::Duration,
};

// TODO: Make this changeable
const PLAYLIST_STORAGE: &str = "./songs.ron";
const UPDATE_RATE: Duration = Duration::from_millis(15);
const FILE_EXTENSIONS: [&str; 9] = [
    "mp3", "3gp", "wav", "wave", "ogg", "oga", "ogv", "ogx",
    "flac", //"mp4", "m4a", "m4p", "m4b", "m4r", "m4v", "aac",
];

fn read_paths_from_file() -> BTreeSet<PathBuf> {
    if !PathBuf::from(PLAYLIST_STORAGE).exists() {
        File::create(PLAYLIST_STORAGE).unwrap();
    }
    let file_content = read_to_string(PLAYLIST_STORAGE).unwrap();
    match ron::from_str(&file_content) {
        Ok(v) => v,
        Err(_) => BTreeSet::new(),
    }
}

fn overwrite_storage_with_paths(paths: &BTreeSet<PathBuf>) {
    let paths = match ron::to_string(paths) {
        Ok(paths) => paths,
        Err(_) => "".to_string(),
    };
    write(PLAYLIST_STORAGE, paths).expect("Cannot write to file system.");
}

fn path_set_to_playlist_map(paths: &mut BTreeSet<PathBuf>) -> BTreeMap<String, Playlist> {
    let mut mistakes = 0;
    let playlists = paths
        .clone()
        .iter()
        .filter_map(|path| {
            Some((
                name_from_path(&path),
                match Playlist::from_path(&path).ok() {
                    Some(playlist) => playlist,
                    None => {
                        paths.remove(path);
                        mistakes += 1;
                        return None;
                    }
                },
            ))
        })
        .collect();
    if mistakes != 0 {
        overwrite_storage_with_paths(&paths);
    }
    playlists
}

type SongError = Result<(), Box<dyn Error>>;

pub enum PlaylistBarStatus {
    Open,
    Closed,
}

// TODO: Make theme changeable
// Maybe TODO: Highlight played playlist
struct SongPlayer {
    current_playlist: Playlist,
    shown_playlist: Playlist,
    paths: BTreeSet<PathBuf>,
    playlists: BTreeMap<String, Playlist>,
    divide: Option<u16>,
    sink: Sink,
    output_stream: OutputStreamHandle,
    _stream: OutputStream,
    played_duration: f64,
    is_repeating_song: bool,
    volume_slider_animation: SlidingAnimation,
    is_removing_playlists: bool,
    volume: f32,
    theme: Theme,
    error: Option<Box<dyn Error>>,
    playlist_bar_animation: SlidingAnimation,
}
impl SongPlayer {
    fn write_path_to_storage(&mut self, path: &PathBuf) {
        self.paths.insert(path.clone());
        self.overwrite_storage();
    }

    // TODO: Maybe make write call only at the end?
    fn overwrite_storage(&self) {
        overwrite_storage_with_paths(&self.paths);
    }

    fn reset_player(&mut self) -> SongError {
        self.sink = Sink::try_new(&self.output_stream)?;
        self.sink.set_volume(self.volume);
        Ok(())
    }

    fn play(&mut self) {
        // FIXME: Allow decoding of mp4
        match self.current_playlist.get_current() {
            Some(song) => {
                let source = song.source.clone();
                self.played_duration = 0.0;
                self.sink.append(source);
            }
            None => {}
        };
    }

    fn play_and_reset_player(&mut self) -> SongError {
        self.is_repeating_song = false;
        self.reset_player()?;
        self.play();
        Ok(())
    }

    // TODO: Try to refactor
    fn play_with_changed_time(&mut self, time: f64) -> SongError {
        match self.current_playlist.get_current() {
            Some(song) => {
                let source = song.source.clone();
                self.played_duration = time;
                let time = Duration::from_secs_f64(time);
                let source = source.skip_duration(time);
                self.reset_player()?;
                self.sink.append(source);
            }
            None => {}
        };
        Ok(())
    }

    fn save_error(&mut self, error: SongError) {
        match error {
            Ok(_) => {}
            Err(e) => {
                self.error = Some(e);
                self.sink.pause();
            }
        }
    }

    fn create_asset<S: Into<String>>(&self, name: S) -> Svg<Renderer<Backend, Theme>> {
        let path_str = format!("./assets/{}.svg", name.into());
        let path = PathBuf::from(path_str);
        Svg::from_path(path)
    }
}
impl Application for SongPlayer {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: ()) -> (Self, Command<Message>) {
        let mut paths = read_paths_from_file();

        let playlists = path_set_to_playlist_map(&mut paths);

        let (stream, output_stream) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&output_stream).unwrap();

        (
            Self {
                current_playlist: Playlist::empty(),
                shown_playlist: Playlist::empty(),
                paths,
                playlists,
                divide: Some(200),
                sink,
                output_stream,
                _stream: stream,
                played_duration: 0.0,
                is_repeating_song: false,
                volume_slider_animation: SlidingAnimation::new(5.5, 100., SlidingState::Increasing),
                is_removing_playlists: false,
                volume: 1.,
                theme: Theme::Dark,
                error: None,
                playlist_bar_animation: SlidingAnimation::new(9., 300., SlidingState::Decreasing),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        "Song Player".to_string()
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::NextSong => {
                self.current_playlist.advance();
                let result = self.play_and_reset_player();
                self.save_error(result);
            }
            Message::PreviousSong => {
                self.current_playlist.back();
                let result = self.play_and_reset_player();
                self.save_error(result);
            }
            Message::ChangeSong(id) => {
                if self.current_playlist != self.shown_playlist {
                    self.current_playlist = self.shown_playlist.clone();
                }
                self.current_playlist.change_current(id);
                let result = self.play_and_reset_player();
                self.save_error(result);
            }
            Message::Pause => {
                if self.sink.is_paused() {
                    self.sink.play()
                } else {
                    self.sink.pause()
                }
            }
            Message::TimeChanged(time) => {
                let result = self.play_with_changed_time(time);
                self.save_error(result)
            }
            Message::ChangeIteration => self.current_playlist.change_state(),
            // FIXME: May not properly work. Look into it
            Message::RepeatSong => self.is_repeating_song = !self.is_repeating_song,
            Message::ChangePlaylist(name) => {
                self.shown_playlist = self.playlists.get(name.as_str()).unwrap().clone()
            }
            Message::OnResize(divide) => self.divide = Some(divide),
            Message::Tick => {
                if self.sink.empty() && self.error.is_none() {
                    if !self.is_repeating_song {
                        self.current_playlist.advance();
                    }
                    self.play();
                }
                if !self.sink.is_paused() {
                    self.played_duration += UPDATE_RATE.as_secs_f64();
                }
                self.volume_slider_animation.change_size();
                self.playlist_bar_animation.change_size();
                if self.playlist_bar_animation.is_active() {
                    self.divide = Some(self.playlist_bar_animation.size() as u16)
                } else if self.divide != Some(self.playlist_bar_animation.size() as u16) {
                    self.playlist_bar_animation
                        .set_size(self.divide.unwrap_or(500) as f32)
                }
            }
            Message::GetPlaylist => {
                // Maybe TODO: Remove unwrap?
                let audio_folder = match dirs::audio_dir() {
                    Some(path) => path,
                    None => dirs::home_dir().unwrap(),
                };
                // FIXME: Causes app to be seen as frozen
                let path = FileDialog::new()
                    .set_location(&audio_folder)
                    .add_filter("Music file", &FILE_EXTENSIONS)
                    .show_open_single_dir()
                    .unwrap();
                match path {
                    Some(path) => {
                        self.write_path_to_storage(&path);
                        let name = name_from_path(&path);
                        match Playlist::from_path(&path) {
                            Ok(playlist) => {
                                self.playlists.insert(name, playlist);
                            }
                            Err(e) => self.error = Some(e),
                        };
                    }
                    None => {}
                };
            }
            Message::PlaylistRemovalState => {
                self.is_removing_playlists = !self.is_removing_playlists
            }
            Message::RemovePlaylist(name) => {
                self.playlists.remove(name.as_str());
                self.paths.retain(|path| {
                    let path = match path.file_name() {
                        Some(path) => path,
                        None => OsStr::new(""),
                    };
                    path != OsStr::new(name.as_str())
                });
                self.overwrite_storage();
                self.is_removing_playlists = false;
            }
            Message::VolumeSliderState => self.volume_slider_animation.start_animation(),
            Message::SetVolume(vol) => {
                self.volume = vol / 100.;
                self.sink.set_volume(self.volume);
            }
            Message::CloseErrorModal => self.error = None,
            Message::UpdatePlaylists => {
                let current_playlist_name = self
                    .playlists
                    .iter()
                    .find_map(|(name, playlist)| {
                        if self.current_playlist == *playlist {
                            Some(name)
                        } else {
                            None
                        }
                    })
                    .unwrap_or(&String::new())
                    .clone();
                let shown_playlist_name = self
                    .playlists
                    .iter()
                    .find_map(|(name, playlist)| {
                        if self.shown_playlist == *playlist {
                            Some(name)
                        } else {
                            None
                        }
                    })
                    .unwrap_or(&String::new())
                    .clone();
                self.playlists = path_set_to_playlist_map(&mut self.paths);
                self.current_playlist = self
                    .playlists
                    .get(&current_playlist_name)
                    .unwrap_or(&Playlist::empty())
                    .clone();
                self.shown_playlist = self
                    .playlists
                    .get(&shown_playlist_name)
                    .unwrap_or(&Playlist::empty())
                    .clone();
                self.error = None;
            }
            Message::PlaylistBarStatus => self.playlist_bar_animation.start_animation(),
            Message::ChangeTheme => {
                self.theme = match self.theme {
                    Theme::Dark => Theme::Light,
                    Theme::Light => Theme::Dark,
                }
            }
        };
        Command::none()
    }

    // TODO: Allow for dynamic ordering based on window size
    fn view(&self) -> iced::Element<'_, Self::Message, Renderer<Backend, Theme>> {
        let text_height = Length::Fixed(30.);
        let button_size = Length::Fixed(30.);
        let column_spacing = 1;
        let control_spacing = 20;
        let control_column_spacing = 5;
        let rule_size = 3;
        let current_song = self.current_playlist.get_current();
        let sidebar_size = Length::Fixed(40.);

        let playlist_option = |name| {
            if self.is_removing_playlists {
                Message::RemovePlaylist(name)
            } else {
                Message::ChangePlaylist(name)
            }
        };
        let playlists = self
            .playlists
            .iter()
            .rev()
            .fold(Column::new(), |col, (name, playlist)| {
                let style = if playlist.is_empty() {
                    ButtonStyle::Inactive
                } else {
                    ButtonStyle::Default
                };
                let mut button = button(&**name)
                    .height(text_height)
                    .width(Length::Fill)
                    .style(style)
                    .on_press(playlist_option(name.to_string()));
                if *playlist == self.shown_playlist {
                    button = button.style(ButtonStyle::Active)
                }
                col.push(column![button, Rule::horizontal(rule_size),])
            })
            .spacing(column_spacing);

        let playlist_adder = button(self.create_asset("add-playlist"))
            .height(button_size)
            .width(button_size)
            .on_press(Message::GetPlaylist);

        let style = match self.is_removing_playlists {
            true => SvgStyle::Destructive,
            false => SvgStyle::Default,
        };

        // TODO: Improve image when removing playlists
        let remove_image = self.create_asset("remove-playlist").style(style);
        let playlist_remover = button(remove_image)
            .height(button_size)
            .width(button_size)
            .on_press(Message::PlaylistRemovalState);

        let playlist_options = row![playlist_adder, playlist_remover].spacing(column_spacing);

        // TODO: Add button to hide this
        let playlist_bar = column![playlists, playlist_options]
            .width(Length::Fill)
            .align_items(Alignment::Center)
            .height(Length::Fill)
            .spacing(control_column_spacing);

        let volume_image = self.create_asset(format!(
            "volume-{}",
            match (self.volume * 100.) as u8 {
                0 => "empty",
                1..=33 => "low",
                34..=67 => "medium",
                68..=100 => "high",
                _ => unreachable!(),
            }
        ));

        let volume_controls = row![
            button(volume_image.width(button_size).height(button_size))
                .on_press(Message::VolumeSliderState),
            if self.volume_slider_animation.size() > 1. {
                // TODO: If max duration == 0; Display a line instead
                container(Slider::new(
                    0.0..=100.,
                    self.volume * 100.,
                    Message::SetVolume,
                ))
                .width(Length::Fixed(self.volume_slider_animation.size()))
            } else {
                container("")
            },
        ]
        .spacing(2)
        .align_items(Alignment::Center);

        let pause_image = self.create_asset(
            if self.sink.is_paused() || self.current_playlist.is_empty() {
                "play"
            } else {
                "pause"
            },
        );
        let song_controls = row![
            button(
                self.create_asset("backward")
                    .width(button_size)
                    .height(button_size)
            )
            .on_press(Message::PreviousSong),
            button(pause_image.width(button_size).height(button_size)).on_press(Message::Pause),
            button(
                self.create_asset("forward")
                    .width(button_size)
                    .height(button_size)
            )
            .on_press(Message::NextSong),
        ]
        .spacing(control_spacing);

        let repeat_image = self.create_asset(format!(
            "repeat-{}",
            match self.is_repeating_song {
                true => "active",
                false => "inactive",
            }
        ));
        let shuffle_image = self.create_asset(format!(
            "shuffle-{}",
            match self.current_playlist.get_state() {
                PlayerState::Loop => "loop",
                PlayerState::Random => "random",
            }
        ));
        let playback_controls = row![
            button(repeat_image.width(button_size).height(button_size))
                .on_press(Message::RepeatSong),
            button(shuffle_image.width(button_size).height(button_size))
                .on_press(Message::ChangeIteration),
        ]
        .spacing(control_spacing);

        let top_command_card = row![
            volume_controls.width(Length::FillPortion(10)),
            Space::with_width(Length::FillPortion(5)),
            song_controls,
            Space::with_width(Length::FillPortion(10)),
            playback_controls
        ];

        let name = match current_song {
            Some(song) => song.name.to_owned(),
            None => String::new(),
        };

        let duration = match current_song {
            Some(song) => song.duration,
            None => self.played_duration,
        };

        let bottom_command_card = column![
            // TODO: Improve theme maybe?
            text(name).height(Length::Fixed(20.)),
            Slider::new(0.0..=duration, self.played_duration, Message::TimeChanged)
                .height(25)
                .step(1.),
        ]
        .align_items(Alignment::Center);

        let command_card = row![
            Space::with_width(Length::FillPortion(30)),
            column![top_command_card, bottom_command_card]
                .spacing(control_column_spacing)
                .width(Length::FillPortion(600)),
            Space::with_width(Length::FillPortion(25)),
        ];

        let songs = scrollable(
            self.shown_playlist
                .songs
                .iter()
                .enumerate()
                .fold(Column::new(), |col, (i, song)| {
                    let mut button = button(&*song.name)
                        .height(text_height)
                        .width(Length::Fill)
                        .on_press(Message::ChangeSong(i));
                    if Some(song) == current_song {
                        button = button.style(ButtonStyle::Active)
                    }
                    let content = column![button, Rule::horizontal(rule_size),];
                    col.push(content)
                })
                .spacing(column_spacing),
        )
        .height(Length::Fill);

        let main_content = column![
            songs,
            Rule::horizontal(20),
            command_card.height(Length::Fixed(120.)),
        ]
        .align_items(Alignment::Center);

        // FIXME: Does not work first time clickig on menu
        let size = self.divide.unwrap_or(500);
        let app = if size > 20 {
            row![Split::new(
                playlist_bar,
                main_content,
                self.divide,
                split::Axis::Vertical,
                Message::OnResize,
            )
            .min_size_second(600)
            .spacing(2.)
            .width(Length::Fill)]
        } else {
            row![main_content]
        };

        // TODO: If song is bad, add option to remove it/rerun playlist getter
        let app = Modal::new(self.error.is_some(), app, move || {
            let card_length = 575.;
            let warning_text_size = 17.;
            let img_size = Length::Fixed(warning_text_size);
            Card::new(
                text("An Error occured in the application."),
                column![
                    text(format!("{}", match self.error.as_ref() {
                        Some(e) => e.to_string(),
                        None => String::new()
                    })).width(Length::Fill),
                    text("Please contact artemis@stuerz.xyz if you believe this to be a bug."),
                ],
            )
            .foot(
                // TODO: Only show this when necessary
                column![
                    button(text("Update all playlists?"))
                        .style(ButtonStyle::Warning)
                        .on_press(Message::UpdatePlaylists)
                        .height(Length::Shrink)
                        .width(Length::Fill),
                    row![
                        self.create_asset("warning")
                            .style(SvgStyle::Warning)
                            .height(Length::Fixed(warning_text_size))
                            .width(img_size),
                        text("Please only do this if you think that this error occured due to changes in the filesystem.")
                            .size(17)
                            .height(Length::Shrink)
                            .width(Length::Fill),
                    ]
                ].spacing(2)
            )
            .max_width(card_length)
            .on_close(Message::CloseErrorModal)
            .into()
        })
        .on_esc(Message::CloseErrorModal);

        let options = column![button(self.create_asset("dark-theme"))
            .width(button_size)
            .height(button_size)
            .on_press(Message::ChangeTheme),];

        let sidebar = column![
            button(self.create_asset("menu"))
                .width(button_size)
                .height(button_size)
                .on_press(Message::PlaylistBarStatus),
            Space::with_height(Length::Fill),
            options,
        ]
        .width(sidebar_size)
        .align_items(Alignment::Center)
        .spacing(5);

        let app = row![sidebar, Rule::vertical(rule_size), app];

        app.into()
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        iced::time::every(UPDATE_RATE).map(|_| Message::Tick)
    }

    fn theme(&self) -> Theme {
        self.theme.clone()
    }
}

#[derive(Debug, Clone)]
enum Message {
    NextSong,
    PreviousSong,
    ChangeSong(usize),
    Pause,
    TimeChanged(f64),
    ChangeIteration,
    RepeatSong,
    ChangePlaylist(String),
    OnResize(u16),
    Tick,
    GetPlaylist,
    PlaylistRemovalState,
    RemovePlaylist(String),
    VolumeSliderState,
    SetVolume(f32),
    CloseErrorModal,
    UpdatePlaylists,
    PlaylistBarStatus,
    ChangeTheme,
}

fn main() {
    SongPlayer::run(Settings {
        window: iced::window::Settings {
            min_size: Some((800, 600)),
            ..Default::default()
        },
        ..Default::default()
    })
    .unwrap();
}
