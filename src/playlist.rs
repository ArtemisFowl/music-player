use crate::FILE_EXTENSIONS;
use lofty::{read_from, AudioFile};
use rand;
use rand::Rng;
use rodio::source::Buffered;
use rodio::{Decoder, Source};
use std::error::Error;
use std::ffi::OsStr;
use std::fs::{read_dir, File};
use std::io::BufReader;
use std::path::PathBuf;

pub type SongDecoder = Decoder<BufReader<File>>;

#[derive(Clone, Copy, PartialEq)]
pub enum PlayerState {
    Loop,
    Random,
}

// TODO: Maybe add trait so that song loading may only be done when enough ressources are available?
#[derive(Clone)]
pub struct Song {
    pub source: Buffered<SongDecoder>,
    pub name: String,
    pub path: PathBuf,
    pub duration: f64,
}
impl Song {
    pub fn from_path(path: PathBuf) -> Result<Self, Box<dyn Error>> {
        let mut file = File::open(&path)?;
        let duration = match read_from(&mut file) {
            Ok(audio) => audio.properties().duration().as_secs_f64(),
            Err(_) => 0.0,
        };

        let file = BufReader::new(File::open(&path)?);
        let source = Decoder::new(file)?.buffered();
        Ok(Self {
            source,
            name: name_from_path(&path),
            path,
            duration,
        })
    }
}
impl PartialEq for Song {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }

    fn ne(&self, other: &Self) -> bool {
        self.path != other.path
    }
}

// TODO: Maybe reevaluate the playlists when cloning?
// TODO: Also, maybe make a holder struct?
#[derive(Clone, PartialEq)]
pub struct Playlist {
    pub songs: Vec<Song>,
    pub id: usize,
    state: PlayerState,
    initial: bool,
}
impl Playlist {
    pub fn new(songs: Vec<Song>) -> Self {
        Self {
            songs,
            id: 0,
            state: PlayerState::Loop,
            initial: false,
        }
    }

    pub fn empty() -> Self {
        Self {
            songs: Vec::new(),
            id: 0,
            state: PlayerState::Loop,
            initial: true,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.songs.is_empty()
    }

    pub fn change_current(&mut self, id: usize) {
        self.id = id;
    }

    pub fn get_current(&self) -> Option<&Song> {
        self.get(self.id)
    }
    pub fn get(&self, id: usize) -> Option<&Song> {
        self.songs.get(id)
    }

    pub fn change_state(&mut self) {
        self.state = match self.state {
            PlayerState::Loop => PlayerState::Random,
            PlayerState::Random => PlayerState::Loop,
        }
    }

    pub fn get_state(&self) -> PlayerState {
        self.state
    }

    pub fn advance(&mut self) {
        match self.state {
            PlayerState::Loop => self.looping_advance(),
            PlayerState::Random => self.random_advance(),
        }
    }

    fn looping_advance(&mut self) {
        self.id = if self.id < self.songs.len().saturating_sub(1) {
            self.id.saturating_add(1)
        } else {
            0
        };
    }
    fn random_advance(&mut self) {
        // TODO: Improve random iteration
        if !self.is_empty() {
            self.id = rand::thread_rng().gen_range(0..self.songs.len());
        }
    }

    pub fn back(&mut self) {
        match self.state {
            PlayerState::Loop => self.looping_back(),
            PlayerState::Random => self.random_advance(),
        }
    }

    fn looping_back(&mut self) {
        self.id = self
            .id
            .checked_sub(1)
            .unwrap_or_else(|| self.songs.len().saturating_sub(1));
    }

    pub fn from_path(path: &PathBuf) -> Result<Self, Box<dyn Error>> {
        let mut playlist = Vec::new();
        for file in read_dir(&path)? {
            let file = file.unwrap();
            let file_path = file.path();
            let file_type = match file_path.extension().clone() {
                Some(file_type) => file_type.to_string_lossy().into_owned(),
                None => continue,
            };
            if FILE_EXTENSIONS.contains(&file_type.as_str()) {
                let song = Song::from_path(file_path)?;
                playlist.push(song);
            }
        }
        Ok(Playlist::new(playlist))
    }
}

pub fn name_from_path(path: &PathBuf) -> String {
    match path.file_stem() {
        Some(path) => path,
        None => OsStr::new(""),
    }
    .to_string_lossy()
    .to_string()
}
