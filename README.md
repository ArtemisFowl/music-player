# music-player


This application is a simple music player written in rust using iced and rodio.

## Download:

### Compilation from source

```
git clone https://gitlab.com/ArtemisFowl/music-player
cd music-player
cargo run --release
```

Or as a one liner:
```
git clone https://gitlab.com/ArtemisFowl/music-player && cd music-player && cargo run --release
```

*Note:* You need git, rustup and nightly rust installed.
*Note:* The compilation times of this project take a long time. It is planned to refactor at least parts to help with this.

## Features:

- [x] Changing of songs and playlists
- [x] Different playback options (random and looping iteration, repeating of songs)
- [x] Volume control
- [x] Time stamp control
- [x] Saved playlists

## Roadmap:
- [ ] Playlist synching
- [ ] Keyboard and bluetooth shortcuts
- [ ] Search functions for songs or playlists
- [ ] MP4 support
- [ ] Proper random iteration of playlists
- [ ] Responsive window based on window size
- [ ] License
- [ ] Refactorization and splitting the app into components

## Ideas:
- Composable playlists
- Custom themes
