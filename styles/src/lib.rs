use iced::{color, widget::*, Background, Vector, application, Color};
use iced_aw::style::*;

const LINE_WIDTH: u16 = 2;

#[derive(Default)]
pub enum ButtonStyle {
    #[default]
    Default,
    Inactive,
    Active,
    Dead,
    Warning
}

#[derive(Default)]
pub enum SvgStyle {
    #[default]
    Default,
    Destructive,
    Warning,
}

#[derive(Clone, Default)]
pub enum Theme {
    Light,
    #[default]
    Dark,
}
impl Theme {
    fn text_color(&self) -> Color {
        match self {
            Self::Dark => color!(205, 205, 205),
            Self::Light => color!(0, 0, 0)
        }
    }

    fn background_color(&self) -> Color {
        match self {
            Self::Dark => Color::from_rgba8(50, 52, 55, 0.1),
            Self::Light => color!(160, 165, 175, 0.1)
        }
    }

    fn line_color(&self) -> Color {
        match self {
            Self::Dark => color!(110, 110, 110),
            Self::Light => color!(115, 120, 130),
        }
    }

    fn svg_color(&self) -> Color {
        match self {
            Self::Dark => color!(150, 150, 150),
            Self::Light => color!(80, 75, 75),
        }
    }

    fn split(&self) -> split::Appearance {
        use split::*;
        Appearance {
            background: None,
            first_background: None,
            second_background: None,
            border_width: 0.,
            border_color: self.line_color(),
            divider_background: Background::Color(self.line_color()),
            divider_border_width: LINE_WIDTH as f32,
            divider_border_color: self.line_color()
        }
    }

    fn scrollable(&self) -> scrollable::Scrollbar {
        use scrollable::*;
        Scrollbar {
            background: None,
            border_radius: 0.,
            border_width: 2.,
            border_color: color!(0, 0, 0),
            scroller: Scroller {
                color: color!(200, 200, 200),
                border_radius: 2.,
                border_width: 4.,
                border_color: color!(190, 190, 190)
            }
        }
    }

    fn slider(&self) -> slider::Appearance {
        use slider::*;
        let color = self.svg_color();

        Appearance {
            rail_colors: (color, color),
            handle: Handle {
                shape: HandleShape::Rectangle {
                    width: 7,
                    border_radius: 5.,
                },
                color,
                border_width: 0.,
                border_color: color!(0, 0, 0),
            },
        }
    }
}
impl application::StyleSheet for Theme {
    type Style = ();

    fn appearance(&self, _: &Self::Style) -> application::Appearance {
        // TODO: Make a light theme
        use application::*;
        Appearance {
            background_color: self.background_color(),
            text_color: color!(150, 150, 150),
        }
    }
}
impl svg::StyleSheet for Theme {
    type Style = SvgStyle;

    fn appearance(&self, style: &Self::Style) -> svg::Appearance {
        let color = Some(match style {
            SvgStyle::Default => self.svg_color(),
            SvgStyle::Destructive => color!(200, 50, 50),
            SvgStyle::Warning => color!(170, 165, 60)
        });
        svg::Appearance { color }
    }
}
impl button::StyleSheet for Theme {
    type Style = ButtonStyle;

    fn active(&self, style: &Self::Style) -> button::Appearance {
        let text_color = match style {
            ButtonStyle::Default => self.text_color(),
            ButtonStyle::Active => self.text_color(),
            ButtonStyle::Inactive => self.text_color(),
            ButtonStyle::Dead => color!(140, 120, 120),
            ButtonStyle::Warning => self.text_color(),
        };

        let background = match style {
            ButtonStyle::Default => None,
            ButtonStyle::Inactive => None,
            ButtonStyle::Active => Some(Background::Color(match self {
                Self::Dark => color!(65, 75, 80),
                Self::Light => color!(120, 120, 120)
            })),
            ButtonStyle::Dead => None,
            ButtonStyle::Warning => Some(Background::Color(match self {
                Self::Dark => color!(100, 95, 95),
                Self::Light => color!(120, 120, 120)
            })),
        };

        button::Appearance {
            shadow_offset: Vector::new(0.0, 0.0),
            background,
            border_radius: 0.,
            border_width: 0.,
            border_color: self.line_color(),
            text_color,
        }
    }
}
impl container::StyleSheet for Theme {
    type Style = ();
    fn appearance(&self, _: &Self::Style) -> container::Appearance {
        use container::*;
        Appearance {
            text_color: None,
            background: None,
            border_radius: 0.,
            border_color: color!(0, 0, 0),
            border_width: 0.,
        }
    }
}
impl slider::StyleSheet for Theme {
    type Style = ();

    fn active(&self, _: &Self::Style) -> slider::Appearance {
        self.slider()
    }

    fn dragging(&self, _: &Self::Style) -> slider::Appearance {
        self.slider()
    }

    fn hovered(&self, _: &Self::Style) -> slider::Appearance {
        self.slider()
    }
}
impl rule::StyleSheet for Theme {
    type Style = ();

    fn appearance(&self, _: &Self::Style) -> rule::Appearance {
        use rule::*;
        Appearance {
            color: self.line_color(),
            width: LINE_WIDTH,
            radius: 0.,
            fill_mode: FillMode::Full,
        }
    }
}
impl split::StyleSheet for Theme {
    type Style = ();

    fn active(&self, _: Self::Style) -> split::Appearance {
        self.split()
    }

    fn dragged(&self, _: Self::Style) -> split::Appearance {
        self.split()
    }

    fn hovered(&self, _: Self::Style) -> split::Appearance {
        self.split()
    }
}
impl text::StyleSheet for Theme {
    type Style = ();
    fn appearance(&self, _: Self::Style) -> text::Appearance {
        use text::*;
        Appearance { color: Some(self.text_color()) }
    }
}
impl modal::StyleSheet for Theme {
    type Style = ();

    fn active(&self, _: Self::Style) -> modal::Appearance {
        use modal::*;
        Appearance { background: Background::Color(self.background_color()) }
    }
}
impl card::StyleSheet for Theme {
    type Style = ();

    fn active(&self, _: Self::Style) -> iced_aw::card::Appearance {
        use iced_aw::card::*;
        let background = Background::Color(match self {
            Self::Dark =>color!(87, 85, 85),
            Self::Light => color!(132, 132, 132)
        });
        let head_background = Background::Color(match self {
            Self::Dark => color!(120, 45, 45),
            Self::Light => color!(155, 50, 50)
        });
        let close_color = match self {
            Self::Dark => color!(175, 177, 175),
            Self::Light => color!(0, 0, 0)
        };

        Appearance {
            background,
            border_radius: 12.5,
            border_width: 0.,
            head_background,
            body_background: background,
            foot_background: background,
            close_color,
            ..Default::default()
        }
    }
}
impl scrollable::StyleSheet for Theme {
    type Style = ();

    fn active(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }

    fn active_horizontal(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }

    fn dragging(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }

    fn dragging_horizontal(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }

    fn hovered(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }

    fn hovered_horizontal(&self, _: &Self::Style) -> scrollable::Scrollbar {
        self.scrollable()
    }
}
